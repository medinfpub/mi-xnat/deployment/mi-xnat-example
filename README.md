# MI XNAT example

MI XNAT example deployment

## Set up ansible controller

```
sudo apt update && sudo apt upgrade -y
sudo apt install git python3-venv -y
python3 -m venv env && source env/bin/activate
pip install ansible
ansible-galaxy collection install git+https://gitlab.gwdg.de/medinfpub/mi-xnat/ansible/collection.git,dev
git clone git@gitlab.gwdg.de:medinfpub/mi-xnat/deployment/mi-xnat-example.git
```

## Install on single node with default settings

```
cd mi-xnat-example
ansible-playbook -i inventory/ playbooks/up.yml
```